#NAME: Shankun Lin
#Date: March 21, 2016
#HOMEWORK-8 (lecture 10)
SRC = src
PROG1 = myls
PROG1_DEP = $(SRC)/$(PROG1).c

all: $(PROG1)

$(PROG1): $(PROG1_DEP)
	gcc -g $(PROG1_DEP) -o $(PROG1)