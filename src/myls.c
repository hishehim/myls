/**********************************************
Author: Shankun Lin (Mike)
Date: March 27, 2016
Description:
    ls command containing the flags -a and -l
***********************************************/

#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <pwd.h>
#include <grp.h>
#include <sys/ioctl.h>

#define ACCESS_TIME_LEN 13
#define PERM_STR_LEN 11
#define MIN_FILE_NAME_LEN 1
#define __PATH_MAX 4096

#define MAX(a, b) a > b ? a : b
#define MIN(a, b) a < b ? a : b
#define IDX_V(r, c, RM) (c * RM) + r

struct opt {
    bool lngLs, all;
};

struct lngLstWid { // long list width table for variable width columns
    int lnkNum, usrNam, grpNam, sizNum;
};

//extern int versionsort(const void *a, const void *b);
static int charToOption(char *optionList);
static int cleanFilLst();
static int cmpStrArr (const void *a, const void *b);
static int digitLen(long num);
static int getFilLst();
static void getTermWidth();
static void getLngLstWid(struct lngLstWid *widTab, size_t *totalBlk);
static int parseArgs(int argc, char *argv[], int dirArgIdx[], int *dirArgCnt);
static bool checkColCount(const size_t colSize, const size_t rowSize,
                          const size_t lenTable[], int *colWidth);
static int strToOption(char *optionStr);
static size_t calcColCount(int **colWidth);
static void listFile();
static void listFile_l();
static void permToStr(char *str, const mode_t mode);
static void sortFilList(int (*sortFunc)(const void *a, const void *b));static void timeToStr(char *str, const time_t atime);


static char curDir[__PATH_MAX] = "./";
static int curDirOffset = 2;

static struct opt options = {false, false};

static char **filList = NULL;
static size_t filListCnt = 0;

static char badOption = '\0';
static bool hasArgs = false;

static size_t minFilNamLen = __PATH_MAX;
static size_t maxFilNamLen = 1;

static int termWidth = 0;


int
main(int argc, char *argv[]) {
    
    int i, optionReturn,
        dirArgCnt = 0;
    int dirArgIdx[argc];
    void (*listFunc)();
    int (*sortFunc)(const void *a, const void *b) = &cmpStrArr;
    
    if ( parseArgs(argc, argv, dirArgIdx, &dirArgCnt) < 0 ) {
        return 0;
    }
    if (options.lngLs) {
        listFunc = &listFile_l;
    } else {
        listFunc = &listFile;
    }
    if (hasArgs == false) {
        getFilLst();
        sortFilList(sortFunc);
        
        listFunc();
        
        cleanFilLst();
    } else {
        /* if there is a list of arguments that are files
            process those first */
        if (filListCnt > 0) {
            sortFilList(sortFunc);
            listFile();
            cleanFilLst();
        }
        // sort directory list TO-DO
        /* Process any directory arguments */
        for (i = 0; i < dirArgCnt; i++) {
            size_t pathLen = strlen(argv[dirArgIdx[i]]) + 1;
            if (pathLen >= __PATH_MAX) {
                printf("myls: pathname too long: %s\n", argv[dirArgIdx[i]]);
                continue;
            }
            strcpy(curDir, argv[dirArgIdx[i]]);
            strncat(curDir, "/", 1);
            curDirOffset = pathLen;
            getFilLst();
            sortFilList(sortFunc);

            printf("%s:\n",argv[dirArgIdx[i]]);
            listFunc();

            cleanFilLst();
            //printf("\n");
        }
    }
    return 0;
}








static int parseArgs(int argc, char *argv[], int dirArgIdx[], int *dirArgCnt)
{
    int i, optionReturn, nonDirCnt = 0;
    int nonDirLst[argc];
    struct stat buf;
        
    for (i = 1; i < argc; i++) {
        if (argv[i][0] == '-') { // argument is a option flag
            if (argv[i][1] == '-') { // option is in words
                if ( strToOption(argv[i]+2) < 0) {
                    printf("%s: unknown option -- %s\n", argv[0], argv[i]);
                    return -1;
                }
            }
            else { // option is regular flag
                if ( charToOption(argv[i]+1) < 0 ) {
                    printf("%s: unknown option -- %c\n", argv[0], badOption);
                    return -1;
                }
            }
        }
        else { // argument is a file name or directory
            hasArgs = true;
            if (lstat(argv[i], &buf) == 0) {
                size_t len = strlen(argv[i]);
                if (S_ISDIR(buf.st_mode)) {
                    dirArgIdx[*dirArgCnt] = i;
                    *dirArgCnt = *dirArgCnt + 1;
                } else {
                    nonDirLst[nonDirCnt] = i;
                    nonDirCnt++;
                    if (len < minFilNamLen) minFilNamLen = len;
                    if (len > maxFilNamLen) maxFilNamLen = len;
                }
            } 
            else {
                printf("%s: cannot access %s: %s\n", argv[0],
                                                     argv[i],
                                                     strerror(errno));
            }
        }
    }
    filListCnt = 0;
    filList = calloc(sizeof(char*), nonDirCnt);
    for (i = 0; i < nonDirCnt; i++) {
        filList[i] = malloc(strlen(argv[nonDirLst[i]])+1);
        strcpy(filList[i], argv[nonDirLst[i]]);
        filListCnt++;
    }
    return 0;
}

static int strToOption(char *optionStr) {
    if (strcmp("all",optionStr) == 0) {
        options.all = true;
        return 0;
    }
    return -1;
}

static int charToOption(char *optionList) {
    if (optionList == NULL) {
        return -1;
    }
    badOption = '\0';
    int i = 0;
    while (optionList[i] != '\0') {
        switch (optionList[i]) {
            case 'a':
                options.all = true;
                break;
            case 'l':
                options.lngLs = true;
                break;
            default:
                badOption = optionList[i];
                return -1;
        }
        i++;
    }
    return 0;
}

static void permToStr(char str[], const mode_t mode) {
    str[0] = S_ISDIR(mode) ? 'd' : '-';
    str[1] = (mode & S_IRUSR) ? 'r' : '-';
    str[2] = (mode & S_IWUSR) ? 'w' : '-';
    str[3] = (mode & S_IXUSR) ? 'x' : '-';
    str[4] = (mode & S_IRGRP) ? 'r' : '-';
    str[5] = (mode & S_IWGRP) ? 'w' : '-';
    str[6] = (mode & S_IXGRP) ? 'x' : '-';
    str[7] = (mode & S_IROTH) ? 'r' : '-';
    str[8] = (mode & S_IWOTH) ? 'w' : '-';
    str[9] = (mode & S_IXOTH) ? 'x' : '-';
    str[10] = '\0';
}

static void timeToStr(char str[], const time_t atime) {
    struct tm *tminfo;
    tminfo = localtime(&atime);
    strftime(str, ACCESS_TIME_LEN, "%b %d %R", tminfo);
}

static int digitLen(long num) {
    if (num == 0) {
        return 1;
    }
    size_t digits = 0;
    while (num > 0) {
        num = num / 10;
        digits++;
    }
    return digits;
}

static void sortFilList(int (*sortFunc)(const void *a, const void *b)) {
    qsort(filList, filListCnt, sizeof(char*), sortFunc);
}

static int cmpStrArr (const void *a, const void *b) {
    return strcasecmp(*(const char**)a, *(const char**)b);
}

static int getFilLst() {
    minFilNamLen = __PATH_MAX;
    maxFilNamLen = 1;
    if (filList != NULL) {
        cleanFilLst();
    }
    
    filList == NULL;
    filListCnt = 0;

    int totalFiles = 0;
    struct dirent* ent;
    DIR* parent;
    
    parent = opendir(curDir);
    if (parent == NULL) {
        printf("myls: cannot access %s: %s\n", curDir, strerror(errno));
        return -1;
    }
    
    // count files
    while ( ent = readdir(parent) )
    {
        if ( (ent->d_name)[0] == '.' && !options.all)
            continue;
        totalFiles++;
    }
    
    rewinddir(parent);
    filList = calloc(sizeof(char*), totalFiles);
    
    // put all file names in an array
    while ( (ent = readdir(parent)) && (filListCnt < totalFiles) ) {
        if ( (ent->d_name)[0] == '.' && !options.all) {
            continue;
        }
        size_t len = strlen(ent->d_name);
        filList[filListCnt] = malloc(len+1);
        strcpy(filList[filListCnt], ent->d_name);
        filListCnt++;
        if (len < minFilNamLen) minFilNamLen = len;
        if (len > maxFilNamLen) maxFilNamLen = len;
    }
    closedir(parent);
    return 0;
}

static int cleanFilLst() {
    int i;
    for (i = 0; i < filListCnt; i++) {
        free(filList[i]);
    }
    free (filList);
    filList = NULL;
    filListCnt = 0;
}

static void getTermWidth() {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    termWidth = w.ws_col;
}

static bool checkColCount(const size_t rowSize, const size_t colSize,
                          const size_t lenTable[], int *colWidth) {
    size_t r, c, total;
        
    /* clear width table by setting to min*/
    for (c = 0; c < colSize; c++)
        colWidth[c] = minFilNamLen + 2;
    
    /* check column table */
    for (r = 0; r < rowSize; r++) {
        size_t rowTotal = 0;
        for (c = 0; c < colSize; c++) {
            size_t idx = IDX_V(r, c, rowSize);
            if (idx >= filListCnt) { break; }
            rowTotal += lenTable[idx];
            if (lenTable[idx] > colWidth[c]) {
                colWidth[c] = lenTable[idx];
            }
            if (rowTotal >= termWidth) {
            return false;
            }
        }
    }
    
    total = 0;
    for (c = 0; c < colSize; c++) {
        total += colWidth[c];
    }
    if ( (total-2) >= termWidth) {
        return false;
    }
    return true;
}

/*
calculate the maximum number of columns possible
colWidth will dereference to NULL if colSize is equal to 1.
    In this case, maxFilNamLen will be used as column width
otherwise colWidth points to an array that specifies the width
    of each column
Return the max number of columns displayable
*/
static size_t calcColCount(int **colWidth) {
    int i;
    size_t col, colMax, colMin;
    size_t row, rowMax, rowMin;
    *colWidth = NULL;
    getTermWidth();
    
    if ( termWidth < (minFilNamLen * 2) ) {
        *colWidth = malloc(sizeof(int));
        **colWidth = maxFilNamLen;
        return 1;
    }
    /* Add two to each file name for '  ' separator */
    colMin = termWidth / (maxFilNamLen + 2);    
    if ( (filListCnt <= colMin) ) {
        *colWidth = calloc(sizeof(int), filListCnt);
        for (i = 0; i < filListCnt; i++)
            (*colWidth)[i] = strlen(filList[i]) + 2;
        return filListCnt;
    }
    colMax = ((termWidth - (maxFilNamLen + 2)) / (minFilNamLen + 2)) + 1;
    
    rowMax = (filListCnt / colMin);
    if (filListCnt % colMin) rowMax++;
    rowMin = (filListCnt / colMax);
    if (filListCnt % colMax) rowMin++;

    /* Pre processed table for quick access */
    size_t lenTable[filListCnt];
    for (i = 0; i < filListCnt; i++)
        lenTable[i] = strlen(filList[i]) + 2;

    /* Test display dimension */
    //col = colMin;
    // do {
    //    free(holder);
    //    holder = calloc(sizeof(int), col);
    //    if (!checkColCount(col, lenTable, holder)) {
    //         col--;
    //         free(*colWidth);
    //         *colWidth = holder;
    //         break;
    //     }
    //     col++;
    // } while (col <= colMax);

    for (row = rowMin; row <= rowMax; row++) {
        col = filListCnt / row;
        if (filListCnt % row) col++;
        free(*colWidth);
        *colWidth = calloc(sizeof(int), col);
        if (checkColCount(row, col, lenTable, *colWidth)) {
            break;
        }
    }
    // for (col = colMax; col >= colMin; col--) {
    //     free(*colWidth);
    //     *colWidth = calloc(sizeof(int), col);
    //     if (checkColCount(col, lenTable, *colWidth)) {
    //         break;
    //     }
    // }
    return col;
}

static void listFile() {
    
    if (filListCnt <= 0) return;
    
    size_t rowSize, colSize, r, c;
    int *colWidth = NULL;
    
    colSize = calcColCount(&colWidth);
    rowSize = filListCnt / colSize;
    if (filListCnt % colSize) rowSize++;
 
    printf("%zd-%zd\n", rowSize, colSize);   
    // remove padding on right most column
    colWidth[colSize-1] += -2;
    
    for (r = 0; r < rowSize; r++) {
        size_t idx;
        for (c = 0; c < colSize; c++) {
            idx = IDX_V(r, c, rowSize);
            if (idx >= filListCnt) {
                break;
            }
            printf("%-*s", colWidth[c], filList[idx]);
        }
        puts("");
    }
    
    free(colWidth);
}

/*
scan all the file and calculate the width of columns with variable length
*/
static void getLngLstWid(struct lngLstWid *widTab, size_t *totalBlk) {
    int i, lnkNumLen, usrNamLen, grpNamLen, sizNumLen;
    struct stat buf;
    struct passwd *pwd = NULL;
    struct group *grp = NULL; 
    char target[__PATH_MAX] = "";
    
    *totalBlk = 0;
    strcpy(target, curDir);
    
    for (i = 0; i < filListCnt; i++) {
        strcpy(target + curDirOffset, filList[i]);
        lstat(target, &buf);
        if ( ((pwd = getpwuid(buf.st_uid)) == NULL) ||
             ((grp = getgrgid(buf.st_gid)) == NULL) ) {
            perror("myls");
            return;
        }
        
        lnkNumLen = digitLen((long)buf.st_nlink);
        usrNamLen = strlen(pwd->pw_name);
        grpNamLen = strlen(grp->gr_name);
        sizNumLen = digitLen((long)buf.st_size);
        
        if (lnkNumLen > widTab->lnkNum) widTab->lnkNum = lnkNumLen;
        if (usrNamLen > widTab->usrNam) widTab->usrNam = usrNamLen;
        if (grpNamLen > widTab->grpNam) widTab->grpNam = grpNamLen;
        if (sizNumLen > widTab->sizNum) widTab->sizNum = sizNumLen;
        *totalBlk = *totalBlk + buf.st_blocks;
    }
}

static void listFile_l() {
    int i;
    size_t totalBlk = 0;
    struct lngLstWid widTab = {1, 1, 1, 1};
    struct stat buf;
    struct passwd *pw;
    struct group *gp;
    char perStr[PERM_STR_LEN] = "",
         atimeStr[ACCESS_TIME_LEN] = "",
         target[__PATH_MAX] = "";
         
    if (filListCnt <= 0) return;
    
    errno = 0;
    getLngLstWid(&widTab, &totalBlk);
    if (errno != 0) {
        return;
    }
    strcpy(target, curDir);
    
    printf("total %zd\n",totalBlk);
    for (i = 0; i < filListCnt; i++) {
        strcpy(target + curDirOffset, filList[i]);
        lstat(target, &buf);
        pw = getpwuid(buf.st_uid);
        gp = getgrgid(buf.st_gid);
        
        if (pw == NULL || gp == NULL) {
            continue;
        }
        
        permToStr(perStr, buf.st_mode);
        timeToStr(atimeStr, buf.st_mtime);
        /* perm. links, usr, grp, size, access time, filename */
        printf("%s %*lu %*s %*s %*lu %s %s\n", perStr,
                                             widTab.lnkNum, (unsigned long)buf.st_nlink,
                                             widTab.usrNam, pw->pw_name,
                                             widTab.grpNam, gp->gr_name,
                                             widTab.sizNum, (unsigned long)buf.st_size,
                                             atimeStr, filList[i]);
    }
    // printf("%d, %d, %d, %d\n", widTab.lnkNum, widTab.usrNam, widTab.grpNam, widTab.sizNum);
}